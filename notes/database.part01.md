1.In the first video, Dr. Chuck gives us a brief history of the evolution of data processing. What examples did he use? Summarize his brief historywith an even briefer one in your own word.In the old days, people didn't have a lot of storage and they used tape. Example he used about a bank and there's account and  you record all the transaction. 

2. What does Dr. Chuck say is the advantage of relational databases that led eventually to their becoming popular?The primary benefit of the relational database approach is the ability to create meaningful information by joining the tables. 
3. "SQL is not a procedural language it is an imperative language" says Dr. Chuck. Do a bit of web research and explain in a bit more detail what he means.
SQL is a database access language that is both simple and powerful. SQL is a non-procedural language, which means that users describe what they want done in SQL, and the SQL language compiler generates a procedure to navigate the database and complete the task. 

4. What is CRUD? Create, Read, Update, and Delete (CRUD) 

5. In a large scale web appliation, the developer and the DBA are often separate roles.  Explain. developer works more closely with a development team to create a database system or use a database system in a new way, while an admin designs databases and database servers and oversees their installation.

6.Who is E.F. Codd and why am I asking this question? (note: this is not covered in Dr. Chuck's lecture). Computer scientist and mathematician who devised the “relational” data model, which led to the creation of the relational database, a standard method of retrieving and storing. He is the creator of the retional databse and that is why you might asking this question.
