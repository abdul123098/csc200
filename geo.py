class Point:
    """Create 2D Points"""

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'({self.x}, {self.y})'
     
    def distance(self, other):
        return ((self.x - other.x)**2 + (self.y - other.y)**2)**0.5

