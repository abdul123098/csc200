import random
count = int(input("How many questions: "))
quest = count
score = 0
while count > 0:
    a = random.randint(0, 15)
    i = random.randint(0, 15)
   
    print("What is " + str(a) + " times " + str(i) + "?")
    ans = int(input("Answer: "))
    if ans ==  (a*i):
        print("That’s right – well done.")
        score += 1
    else:
        print("No, I’m afraid the answer is " + str(a*i))
        count -= 1

print("I asked you",quest,"questions. You got",score,"of them right. Well done!")


