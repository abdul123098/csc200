from gasp import *
import time
GRID_SIZE = 30
MARGIN = GRID_SIZE
BACKGROUND_COLOR = color.BLACK
WALL_COLOR = "#99E5E5"
class Immovable:
    pass

class Nothing(Immovable):
    pass

class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        (screen_x, screen_y) = self.screen_point
        dot_size = GRID_SIZE * 0.2
        Circle(self.screen_point, dot_size, color=WALL_COLOR, filled=True)


class Maze:
    def __init__(self):
        self.have_window = False
        self.game_over = False
        self.get_level()

    def get_level(self):
        f = open("layout.dat")
        self.the_layout = []
        for line in f.readlines():
            self.the_layout.append(line.rstrip())

    def set_layout(self):
        height = len(layout)
        width = len(layout[0])
        self.make_window(width, height)
        self.make_map(width, height)

        max_y = height - 1
        for x in range(width):
            for y in range(height):
                char = layout[max_y - y][x]
                self.make_object((x, y), char)


    def finished(self):
        return self.game_over

    def play(self):
        answered = input("Are we done yet? ")
        if answered == 'y':
            self.game_over = True
        else:
            print("I'm Playing")

    def done(self):
        print("I'm not playing anymorex")
        end_graphics()
        self.map = []


    def make_window(self, width, height):
        grid_width = (width-1) * GRID_SIZE
        grid_height = (height-1) * GRID_SIZE
        screen_width = 2*MARGIN + grid_width
        screen_height = 2*MARGIN + grid_height
        begin_graphics(screen_width, screen_height, "chomp", BACKGROUND_COLOR)

    def to_screen(self, point):
        (x, y) = point
        x = x*GRID_SIZE + MARGIN
        y = y*GRID_SIZE + MARGIN
        return (x, y)

    def make_map(self, width, height):
        self.width = width
        self.height = height
        self.map = []
        for y in range(height):
            new_row = []
            for x in range(width):
                new_row.append(Nothing())
            self.map.append(new_row)

    def make_object(self, point, character):
        (x, y) = point
        if character == '%':
            self.map[y][x] = Wall(self, point)


the_maze = Maze()
while not the_maze.finished():
    the_maze.play()

the_maze.done()

